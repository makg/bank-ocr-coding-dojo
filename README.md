# Bank OCR

http://codingdojo.org/kata/BankOCR/#resources

## How to run?

Using Docker:

```
docker build -t bank-ocr .
docker run -v /path/to/project/tests/account_numbers.txt:/accounts.txt bank-ocr
```

Without Docker:

```
composer install
php bin/console bank-ocr tests/account_numbers.txt
```

## Short description of classes

- `src/OCR/Reader.php` - reads a single scanned number, not necessarily an account number
- `src/AccountNumberParser/AccountNumberReader.php` - uses a generic OCR and wraps a result as the `AccountNumber`
- `src/AccountNumberParser/Parser.php` - parses a scanned number (uses OCR and applies additional logic - guessing correct account numbers)
- `src/AccountNumberParser/Formatter/ParserResultFormatter.php` - formats the output to indicate account numbers with illegal characters, wrong checksums etc.
- `src/AccountNumberParser/FileParser.php` - reads an entire text file with scanned account numbers and parses them
- `src/AccountNumberParser/Result/AccountNumber.php` - stores and validates the account number
