<?php

namespace App\OCR;


use App\OCR\Exception\InvalidInputException;
use App\OCR\Result\Character;
use App\OCR\Result\RecognizedString;

class Reader implements ReaderInterface
{
    public const CHARACTER_WIDTH = 3;
    public const CHARACTER_HEIGHT = 3;

    public function read(string $input): RecognizedString
    {
        $inputLines = explode(PHP_EOL, trim($input, PHP_EOL));

        $this->validateInputLines($inputLines);

        $characters = [];
        $charactersCount = strlen($inputLines[0]) / self::CHARACTER_WIDTH;

        for ($currentCharacter = 0; $currentCharacter < $charactersCount; $currentCharacter++) {
            $characterString = '';

            foreach ($inputLines as $line) {
                $characterString .= substr($line, $currentCharacter * self::CHARACTER_WIDTH, self::CHARACTER_WIDTH);
            }

            $characters[] = new Character($characterString);
        }

        return new RecognizedString($characters);
    }

    private function validateInputLines(array $inputLines): void
    {
        if (count($inputLines) !== self::CHARACTER_HEIGHT) {
            throw new InvalidInputException('Invalid character height.');
        }


        $expectedCharactersCount = strlen($inputLines[0]);

        foreach ($inputLines as $lineNumber => $line) {
            if (!empty($line) && (strlen($line) !== $expectedCharactersCount || strlen($line) % self::CHARACTER_WIDTH !== 0)) {
                throw new InvalidInputException(sprintf('Invalid length in line %s.', (int)$lineNumber + 1));
            }
        }
    }
}
