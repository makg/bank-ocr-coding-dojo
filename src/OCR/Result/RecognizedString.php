<?php

namespace App\OCR\Result;


class RecognizedString
{
    /** @var Character[] */
    private $characters = [];

    public function __construct(array $characters)
    {
        $this->characters = $characters;
    }

    /**
     * @return Character[]
     */
    public function getCharacters(): array
    {
        return $this->characters;
    }

    public function getLength(): int
    {
        return count($this->getCharacters());
    }

    public function hasIllegalCharacter(): bool
    {
        foreach ($this->getCharacters() as $character) {
            if (null === $character->getCharacter()) {
                return true;
            }
        }

        return false;
    }

    public function __toString()
    {
        $string = '';

        foreach ($this->getCharacters() as $character) {
            $string .= (string)$character;
        }

        return $string;
    }
}
