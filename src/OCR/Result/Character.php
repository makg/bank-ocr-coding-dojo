<?php

namespace App\OCR\Result;


class Character
{
    private const CHARACTER_MAP = [
        0 => ' _ '.'| |'.'|_|',
        1 => '   '.'  |'.'  |',
        2 => ' _ '.' _|'.'|_ ',
        3 => ' _ '.' _|'.' _|',
        4 => '   '.'|_|'.'  |',
        5 => ' _ '.'|_ '.' _|',
        6 => ' _ '.'|_ '.'|_|',
        7 => ' _ '.'  |'.'  |',
        8 => ' _ '.'|_|'.'|_|',
        9 => ' _ '.'|_|'.' _|',
    ];

    /** @var string|null */
    private $character = null;

    public function __construct(string $renderedCharacter)
    {
        $renderedCharacter = str_replace(PHP_EOL, '', $renderedCharacter);
        $foundCharacter = array_search($renderedCharacter, self::CHARACTER_MAP, true);

        if (false !== $foundCharacter) {
            $this->character = (string)$foundCharacter;
        }
    }

    public function getCharacter(): ?string
    {
        return $this->character;
    }

    public function __toString()
    {
        return $this->getCharacter() ?? '?';
    }
}
