<?php

namespace App\OCR;


use App\OCR\Result\RecognizedString;

interface ReaderInterface
{
    public function read(string $input): RecognizedString;
}
