<?php

namespace App\AccountNumberParser\Result;


class ParserResult
{
    /** @var AccountNumber */
    private $accountNumber;

    /** @var AccountNumber[] */
    private $otherPossibilities;

    public function __construct(AccountNumber $accountNumber, array $otherPossibilities = [])
    {
        $this->accountNumber = $accountNumber;
        $this->otherPossibilities = $otherPossibilities;
    }

    /**
     * @return AccountNumber
     */
    public function getAccountNumber(): AccountNumber
    {
        return $this->accountNumber;
    }

    /**
     * @return AccountNumber[]
     */
    public function getOtherPossibilities(): array
    {
        return $this->otherPossibilities;
    }

    public function hasAnyOtherPossibleResult(): bool
    {
        return count($this->getOtherPossibilities()) > 0;
    }
}
