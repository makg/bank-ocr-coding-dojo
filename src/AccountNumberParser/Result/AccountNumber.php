<?php

namespace App\AccountNumberParser\Result;


use App\OCR\Result\RecognizedString;

class AccountNumber extends RecognizedString
{
    public const REQUIRED_LENGTH = 9;
    private const VALID_CHECKSUM = 0;

    public function isValid(): bool
    {
        if ($this->getLength() !== self::REQUIRED_LENGTH) {
            return false;
        }

        if ($this->hasIllegalCharacter()) {
            return false;
        }

        if (!$this->hasValidChecksum()) {
            return false;
        }

        return true;
    }

    public function hasValidChecksum(): bool
    {
        return $this->getChecksum() === self::VALID_CHECKSUM;
    }

    public function getChecksum(): int
    {
        $sum = 0;
        $reversedCharacters = array_reverse($this->getCharacters());

        foreach ($reversedCharacters as $position => $character) {
            $sum += (int)(string)$character * ($position + 1);
        }

        return $sum % 11;
    }
}
