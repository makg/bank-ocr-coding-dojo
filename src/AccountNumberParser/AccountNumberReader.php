<?php

namespace App\AccountNumberParser;


use App\AccountNumberParser\Result\AccountNumber;
use App\OCR\Reader;
use App\OCR\ReaderInterface;
use App\OCR\Result\RecognizedString;

class AccountNumberReader implements ReaderInterface
{
    /** @var ReaderInterface */
    private $reader;

    public function __construct(ReaderInterface $reader)
    {
        $this->reader = $reader;
    }

    public function read(string $input): RecognizedString
    {
        $recognizedString = $this->reader->read($input);

        return new AccountNumber($recognizedString->getCharacters());
    }
}
