<?php

namespace App\AccountNumberParser;


use App\AccountNumberParser\Result\AccountNumber;
use App\AccountNumberParser\Result\ParserResult;
use App\OCR\ReaderInterface;

class Parser
{
    /** @var ReaderInterface */
    private $reader;

    public function __construct(ReaderInterface $accountNumberReader)
    {
        $this->reader = $accountNumberReader;
    }

    public function parse(string $input): ParserResult
    {
        /** @var AccountNumber $accountNumber */
        $accountNumber = $this->reader->read($input);
        $otherPossibilities = [];

        if (!$accountNumber->isValid()) {
            $otherPossibilities = $this->findDifferentMatches($input);
        }

        return new ParserResult($accountNumber, $otherPossibilities);
    }

    private function findDifferentMatches(string $input): array
    {
        $matches = [];
        // TODO normalize input string somewhere

        foreach (str_split($input) as $charIndex => $character) {
            if ($character === PHP_EOL) {
                continue;
            }

            foreach ($this->getReplacementsForCharacter($character) as $replacement) {
                $modifiedInput = substr_replace($input, $replacement, $charIndex, 1);

                /** @var AccountNumber $accountNumber */
                $accountNumber = $this->reader->read($modifiedInput);

                if ($accountNumber->isValid()) {
                    $matches[] = $accountNumber;
                }
            }
        }

        return $matches;
    }

    private function getReplacementsForCharacter(string $character): array
    {
        if ($character === ' ') {
            return ['_', '|'];
        }

        return [' '];
    }
}
