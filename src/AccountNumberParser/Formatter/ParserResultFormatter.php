<?php

namespace App\AccountNumberParser\Formatter;


use App\AccountNumberParser\Result\AccountNumber;
use App\AccountNumberParser\Result\ParserResult;

class ParserResultFormatter implements FormatterInterface
{
    public function format(ParserResult $parserResult): string
    {
        $accountNumber = $parserResult->getAccountNumber();
        $otherPossibilities = $parserResult->getOtherPossibilities();

        if (!$accountNumber->hasValidChecksum() && !$parserResult->hasAnyOtherPossibleResult()) {
            return sprintf('%s ERR', (string)$accountNumber);
        }

        if ($accountNumber->hasIllegalCharacter() && !$parserResult->hasAnyOtherPossibleResult()) {
            return sprintf('%s ILL', (string)$accountNumber);
        }

        if (!$accountNumber->isValid() && count($otherPossibilities) > 1) {
            return sprintf('%s AMB [%s]', (string)$accountNumber, $this->getOtherPossibilitiesAsString($otherPossibilities));
        }


        if (count($otherPossibilities) === 1) {
            $accountNumber = $otherPossibilities[0];
        }

        return (string)$accountNumber;
    }

    private function getOtherPossibilitiesAsString(array $otherPossibilities): string
    {
        return implode(', ', array_map(function (AccountNumber $accountNumber) {
            return (string)$accountNumber;
        }, $otherPossibilities));
    }
}
