<?php

namespace App\AccountNumberParser\Formatter;


use App\AccountNumberParser\Result\ParserResult;

interface FormatterInterface
{
    public function format(ParserResult $parserResult): string;
}
