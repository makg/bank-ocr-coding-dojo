<?php

namespace App\AccountNumberParser;


use App\AccountNumberParser\Exception\InvalidFileException;
use App\AccountNumberParser\Formatter\FormatterInterface;
use App\AccountNumberParser\Result\AccountNumber;
use App\OCR\Reader;

class FileParser
{
    private const ENTRY_HEIGHT = 4;

    /** @var Parser */
    private $parser;

    /** @var FormatterInterface */
    private $formatter;

    public function __construct(Parser $parser, FormatterInterface $formatter)
    {
        $this->parser = $parser;
        $this->formatter = $formatter;
    }

    public function parse(string $filename): string
    {
        $lines = file($filename, FILE_IGNORE_NEW_LINES);

        if (false === $lines) {
            throw new InvalidFileException('Could not read the file.');
        }

        $scannedNumbers = $this->extractScannedNumbers($lines);
        $output = '';

        foreach ($scannedNumbers as $scannedNumber) {
            $parserResult = $this->parser->parse($scannedNumber);

            $output .= $this->formatter->format($parserResult) . PHP_EOL;
        }

        return $output;
    }

    private function extractScannedNumbers(array $lines): array
    {
        $numbers = [];
        $currentNumber = '';

        for ($lineIndex = 0; $lineIndex < count($lines); $lineIndex++) {
            $line = $this->normalizeEmptyLine($lines[$lineIndex]);

            if ($lineIndex % self::ENTRY_HEIGHT === 3) {
                continue;
            }

            if (!empty($currentNumber) && $lineIndex % self::ENTRY_HEIGHT === 0) {
                $numbers[] = $currentNumber;
                $currentNumber = '';
            }

            $currentNumber .= $line . PHP_EOL;
        }

        return $numbers;
    }

    private function normalizeEmptyLine(string $line): string
    {
        if (empty($line)) {
            return str_pad('', AccountNumber::REQUIRED_LENGTH * Reader::CHARACTER_WIDTH, ' ', STR_PAD_LEFT);
        }

        return $line;
    }
}
