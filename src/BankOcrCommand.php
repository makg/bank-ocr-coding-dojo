<?php

namespace App;


use App\AccountNumberParser\FileParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BankOcrCommand extends Command
{
    /** @var FileParser */
    private $fileParser;

    public function __construct(FileParser $fileParser)
    {
        parent::__construct(null);

        $this->fileParser = $fileParser;
    }

    protected function configure(): void
    {
        $this
            ->setName('bank-ocr')
            ->addArgument('filename', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var string $filename */
        $filename = $input->getArgument('filename');

        $result = $this->fileParser->parse($filename);

        $output->write($result);

        return 0;
    }
}
