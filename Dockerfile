FROM composer:1.9

COPY . /app

RUN composer install --no-dev

ENTRYPOINT ["php", "/app/bin/console", "bank-ocr", "/accounts.txt"]
