<?php


use App\AccountNumberParser\AccountNumberReader;
use App\AccountNumberParser\FileParser;
use App\AccountNumberParser\Formatter\ParserResultFormatter;
use App\AccountNumberParser\Parser;
use App\OCR\Reader;
use PHPUnit\Framework\TestCase;

class FileParserTest extends TestCase
{
    public function testParse()
    {
        $accountNumberReader = new AccountNumberReader(new Reader());
        $scannedNumberParser = new Parser($accountNumberReader);
        $formatter = new ParserResultFormatter();

        $fileParser = new FileParser($scannedNumberParser, $formatter);

        $output = $fileParser->parse(realpath(__DIR__ . '/../account_numbers.txt'));

        $this->assertEquals(trim($this->getExpectedOutput()), trim($output));
    }

    private function getExpectedOutput(): string
    {
        return
'000000000
711111111
222222222 ERR
333393333
444444444 ERR
555555555 AMB [559555555, 555655555]
666666666 AMB [686666666, 666566666]
777777177
888888888 AMB [888886888, 888888880, 888888988]
999999999 AMB [993999999, 999959999, 899999999]
123456789
000000051
49006771? ERR
1234?678? ERR
711111111
777777177
200800000
333393333
888888888 AMB [888886888, 888888880, 888888988]
555555555 AMB [559555555, 555655555]
666666666 AMB [686666666, 666566666]
999999999 AMB [993999999, 999959999, 899999999]
490067715 AMB [490067115, 490867715, 490067719]
123456789
000000051';
    }
}
