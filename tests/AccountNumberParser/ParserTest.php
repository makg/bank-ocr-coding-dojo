<?php

use App\AccountNumberParser\AccountNumberReader;
use App\AccountNumberParser\Parser;
use App\OCR\Reader;
use PHPUnit\Framework\TestCase;


class ParserTest extends TestCase
{
    /**
     * @dataProvider parserTestCases
     */
    public function testParse(string $scannerNumber, string $expectedRecognizedNumber, string $expectedGuessedNumber)
    {
        $reader = new AccountNumberReader(new Reader());
        $parser = new Parser($reader);

        $result = $parser->parse($scannerNumber);

        $this->assertEquals($expectedRecognizedNumber, (string)$result->getAccountNumber());
        $this->assertEquals($expectedGuessedNumber, (string)$result->getOtherPossibilities()[0]);
    }

    public function parserTestCases()
    {
        yield 'Account number with invalid 0' => [
            '
 _     _  _  _  _  _  _    
| || || || || || || ||_   |
|_||_||_||_||_||_||_| _|  |',
            '0?0000051',
            '000000051',
        ];

        yield 'Account number with invalid 5' => [
            '
    _  _  _  _  _  _     _ 
|_||_|| ||_||_   |  |  | _ 
  | _||_||_||_|  |  |  | _|',
            '49086771?',
            '490867715',
        ];
    }
}
